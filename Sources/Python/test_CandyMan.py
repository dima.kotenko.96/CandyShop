import unittest

from CandyMan import CandyMan, CandyManError


class TestCandyMan(unittest.TestCase):

    def setUp(self):
        self.candyMan = CandyMan(False)

    def test_receive_one_order_should_complete_without_error(self):
        # Given & When
        order_received = self.candyMan.receive_order()
        # Then
        self.assertEqual(order_received, None)

    def test_receive_two_orders_should_complete_with_error(self):
        # Given
        self.candyMan = CandyMan(True)
        # When
        order_received = self.candyMan.receive_order()
        # Then
        self.assertEqual(order_received, CandyManError.OrderInProgress)

    def test_receive_money_should_complete_without_error(self):
        # Given
        self.candyMan = CandyMan(True)
        # When
        order_received = self.candyMan.receive_money()
        # Then
        self.assertEqual(order_received, None)

    def test_receive_money_without_order_should_complete_with_error(self):
        # Given & When
        order_received = self.candyMan.receive_money()
        # Then
        self.assertEqual(order_received, CandyManError.NoOrdersReceived)


if __name__ == '__main__':
    unittest.main()
