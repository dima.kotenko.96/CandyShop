

class CandyManError:
    OrderInProgress = 1
    NoOrdersReceived = 2


class CandyMan:
    __orderInProgress = False

    def receive_order(self):
        if self.__orderInProgress:
            return CandyManError.OrderInProgress
        self.__orderInProgress = True
        return None

    def receive_money(self):
        if self.__orderInProgress == False:
            return CandyManError.NoOrdersReceived
        self.__orderInProgress = False
        return None

    def __init__(self, orderInProgress):
        self.__orderInProgress = orderInProgress
